const express = require("express");
const router = express.Router();
const Gig = require('../models/Gigs');
const bodyParser = require('body-parser');
router.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
router.use(bodyParser.urlencoded({ extended: true }));
// Get Gigs
router.get('/',(req,res)=>{
    Gig.findAll().then(gigs => {
        console.log(gigs) ;
        res.render('gigs',{
            gigs
        });
      
    }

    ).catch((err)=>{
        res.send(err);
    })
   
})

router.get('/add',(req,res)=>{

    res.render('addgigs');
})



// Add a gig
router.post('/add', (req,res)=>{
    console.log(res);
    // const data = {
    //     title : 'Looking for a react developer',
    //     technology : 'react, mvc, boostrap',
    //     budget : '300 $',
    //     description :'He should be good He should be good He should be good He should be good  He should be good',
    //     email : 'avaishnaw@its.jnj.com'
    // }
    let {title, technologies, budget, description, contact_email} = req.body;
    Gig.create({
        title : title,
        technology : technologies,
        budget : budget,
        description : description,
        email : contact_email
    
    
    }).then((gig)=> {
        res.redirect('/gigs');
    }).catch((err)=> {
        res.send(err);
    })



})



module.exports = router;