const express = require("express");
const expresshb = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require('path');

const db = require('./config/database');
const app = express();



// Handlebars
app.engine('handlebars', expresshb({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.use(express.static(path.join(__dirname,'public')));
const port = process.env.port || 5000;

// gig routes
//app.use('/gigs', require('./routes/gigs'));
// support parsing of application/json type post data
//const router = require('./routes/gigs');

app.use('/.netlify/functions/api',router)
app.get('/',(req,resp) => {

    resp.render("index", { layout: 'landing'} );
})
app.listen(port, console.log(`Server started at port ${port}`))

